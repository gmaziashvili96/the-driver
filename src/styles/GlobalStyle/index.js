import {createGlobalStyle } from 'styled-components';
import PoppinsSemiBoldTTF from '../../Fonts/Poppins-SemiBold.ttf';
import PoppinsRegularTTF from '../../Fonts/Poppins-Regular.ttf';
import PoppinsMediumTTF from '../../Fonts/Poppins-Medium.ttf';

const GlobalStyle = createGlobalStyle`
    *{
        box-sizing:border-box;
        margin:0;
    }
    body {
        background:#E4EBE9;
    }
    a{
        text-decoration:none;
        color:inherit;
    }
    @font-face {
        font-family: "semibold";
        src: local("semibold"),
          url("${PoppinsSemiBoldTTF}") format("truetype");
        font-weight: normal;
    }
    @font-face {
        font-family: "regular";
        src: local("regular"),
          url("${PoppinsRegularTTF}") format("truetype");
        font-weight: normal;
    }
    @font-face {
        font-family: "medium";
        src: local("medium"),
          url("${PoppinsMediumTTF}") format("truetype");
        font-weight: normal;
    }
`;
 
export default GlobalStyle;
