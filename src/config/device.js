export const size = {
    desktopM:1300,
    tablet:1024,
    mobile:760,
}
const device = {
    desktopM:`(max-width: ${size.desktopM}px)`,
    tablet:`(max-width: ${size.tablet}px)`,
    mobile:`(max-width: ${size.mobile}px)`,
}
export default device;