import './Fonts/fonts.css';
import Header from './Components/Header';
import Modal from './Components/Modal';
import Registration from './Components/Registration';
import SignIn from './Components/SignIn';
import QRcode from './Components/QRcode';
import ForgetPassword from './Components/ForgetPassword';
import Home from './pages/Home';
import AboutUs from './pages/About-us';
import ContactUs from './pages/Contact-us';
import FAQ from './pages/FAQ';
import OurCars from './pages/Our-сars';
import GlobalStyle from './styles/GlobalStyle';
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import Settings from './pages/Settings';
import OrderHistory from './pages/Order-history';
import OrderPage from './pages/Order-page';

function App() {
  return(
    <Router>
      <GlobalStyle />
      <Header/>
      <Route path="/about-us">
        <AboutUs />
      </Route>
      <Route path="/contact-us">
        <ContactUs />
      </Route>
      <Route path="/FAQ">
        <FAQ />
      </Route>
      <Route path="/our-cars">
        <OurCars />
      </Route>
      <Route exact path="/settings">
        <Settings/>
      </Route>
      <Route exact path="/order-history">
        <OrderHistory/>
      </Route>
      <Route exact path="/order-page">
        <OrderPage/>
      </Route>
      <Route exact path="/">
        <Home />
      </Route>
      {/* Uncomment for popups */}
      {/* <Modal>
        <Registration/> 
        <SignIn/>
        <ForgetPassword/>
      </Modal> */}
      {/* Uncomment for popups */}
      {/* <Modal maxWidth='320px'>
        <QRcode/>
      </Modal> */}
    </Router>
  )
}

export default App;
