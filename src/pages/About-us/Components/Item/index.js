import styled from 'styled-components';
import device from '../../../../config/device.js';

const Image = styled.img`
    width:550px;
    height:700px;
    border-radius:20px;
    object-fit:cover;
    object-position:center;
    @media ${device.desktopM}{
        width:450px;
        height:600px;
    }
    @media ${device.tablet}{
        width:200px;
        height:auto;
        object-position:right;
        border-radius:15px;
    }
    @media ${device.mobile}{
        width:100%;
        height:200px;
        object-position:center;
        border-radius:5px;
        margin-bottom:15px;
    }
`;

const Info = styled.div`
    width:100%;
    margin-left:100px;
    @media ${device.desktopM}{
        margin-left:40px;
    }
    @media ${device.mobile}{
        margin:0px;
    }
`;

const Title = styled.h2`
    font:30px medium;
    color:#4F4F4F;
    margin-bottom:8px;
    @media ${device.tablet}{
        font-size:24px;
    }
`;

const Desc = styled.div`
    max-width:1065px;
    font:14px medium;
    color:#8A8D8C;
    line-height:28px;
    @media ${device.tablet}{
        font-size:12px;
        line-height:20px;
    }
`;

const Main = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    margin-bottom:120px;
    @media ${device.desktopM}{
        margin-bottom:60px;
    }
    @media ${device.tablet}{
        align-items:stretch;
    }
    @media ${device.mobile}{
        flex-direction:column;
    }
    &:nth-child(2n){
        display: flex;
        flex-direction:row-reverse;
        @media ${device.mobile}{
            flex-direction:column;
        }
        ${Info}{
            margin:0;
            margin-right:100px;
            @media ${device.desktopM}{
                margin-right:40px;
            }
            @media ${device.mobile}{
                margin:0px;
            }
        }
        ${Title},${Desc}{
            text-align:right;
            @media ${device.mobile}{
                text-align:left;
            }
        }
    }
`;

const Item = (props) => {
    const {title,desc,img} = props;
    return(
        <Main>
            <Image src={img}/>
            <Info>
                <Title>{title}</Title>
                <Desc>{desc}</Desc>
            </Info>
        </Main>
    )
}

export default Item;