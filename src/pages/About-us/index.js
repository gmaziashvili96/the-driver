import styled from 'styled-components';
import Wrapper from './../../Components/Wrapper';
import Item from "./Components/Item";
import aboutImg1 from './img/item-img1.jpg';
import aboutImg2 from './img/item-img2.jpg';
import device from './../../config/device.js';
import Footer from '../../Components/Footer';
import React from 'react';
const Info = styled.div`
    display:flex;
    align-items:center;
    flex-direction:column;
    padding:220px 0 180px 0;
    @media ${device.desktopM}{
        padding:150px 0 100px 0;
    }
    @media ${device.tablet}{
        padding:150px 0 50px 0;
    }
`;
const Title = styled.h2`
    font:30px medium;
    color:#4F4F4F;
    margin-bottom:8px;
    text-align:center;
    text-transform:uppercase;
    @media ${device.tablet}{
        font-size:24px;
    }
`;
const Desc = styled.div`
    max-width:1065px;
    font:14px medium;
    color:#8A8D8C;
    line-height:28px;
    text-align:center;
    @media ${device.tablet}{
        font-size:12px;
        line-height:20px;
    }
`;


const AboutUs = () => {
    const infoArr = [
        {
            title:'Taxis have been regulated inGeorgia, all taxis will be white from today.',
            desc:"Pellentesque non tempor neque. Quisque ac purus nunc. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus dapibus sollicitudin vestibulum. Nunc rutrumurna non interdum finibus, leo magna dapibus lorem, sed mollis dolor orci at nisi.There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. Praesent varius nulla ac lorem dictum consectetur. Integer et tortor viverra, feugiat velit non, varius metus. Phasellus consectetur vulputate lacus, vitae lobortis lorem mollis ut. Nulla tincidunt lorem laoreet, pretium urna in, auctor sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vitae diam varius, venenatis ligula ut, lobortis ante. Sed fringilla leo nec ligula fringilla, eget lacinia lacus maximus. Fusce ullamcorper justo non imperdiet molestie. Suspendisse eleifend erat dui. In fermentum tristique libero ac varius. Phasellus sollicitudin tempus purus quis consequat.",
            img:aboutImg1,
        },
        {
            title:'Landmarks and Obligations of our Company We  create all about comfortable taxi',
            desc:"Pellentesque non tempor neque. Quisque ac purus nunc. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus dapibus sollicitudin vestibulum. Nunc rutrumurna non interdum finibus, leo magna dapibus lorem, sed mollis dolor orci at nisi.There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. Praesent varius nulla ac lorem dictum consectetur. Integer et tortor viverra, feugiat velit non, varius metus. Phasellus consectetur vulputate lacus, vitae lobortis lorem mollis ut. Nulla tincidunt lorem laoreet, pretium urna in, auctor sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vitae diam varius, venenatis ligula ut, lobortis ante. Sed fringilla leo nec ligula fringilla, eget lacinia lacus maximus. Fusce ullamcorper justo non imperdiet molestie. Suspendisse eleifend erat dui. In fermentum tristique libero ac varius. Phasellus sollicitudin tempus purus quis consequat.",
            img:aboutImg2,
        },
    ]
    return(
        <React.Fragment>
            <Wrapper>
                <Info>
                    <Title>About our company</Title>
                    <Desc>Vivamus euismod odio eget venenatis ornare. Praesent ut iaculis nibh. Curabitur suscipit sem a sem pellentesque pretium. Fusce id malesuada purus. Nulla in rhoncus sem. Nam sollicitudin gravida lacus nec pellentesque. Vestibulum a imperdiet lacus. Pellentesque in diam at sapien blandit cursus. Etiam at ante nec metus luctus elementum.</Desc>
                </Info>
                <div>
                    {infoArr.map( item => {
                        return <Item key={item.img+=1}
                            title={item.title}
                            desc={item.desc}
                            img={item.img}
                        />
                    })}
                </div>
            </Wrapper>
            <Footer/>
        </React.Fragment>
    )
}

export default AboutUs;