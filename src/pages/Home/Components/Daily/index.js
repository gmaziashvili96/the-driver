
import styled from 'styled-components';
import Wrapper from '../../../../Components/Wrapper';
import CurrencyBox from '../CurrencyBox';
import WeatherBox from '../WeatherBox';
import rusImg from '../../img/rus.png';
import usdImg from '../../img/eur.png';
import eurImg from '../../img/usd.png';
import weatherNightIcon from '../../img/weather/1.svg';
import celcius from '../../img/celcius.svg';
import device from '../../../../config/device.js';

const WrapperExt = styled(Wrapper)`
    display:flex;
    justify-content:space-between;
    align-items:stretch;
    @media ${device.tablet}{
        flex-direction:column;
    }
`;
const Section = styled.div`
    width:100%;
    padding:125px 0;
    @media ${device.desktopM}{
        padding:100px 0;
    }
    @media ${device.mobile}{
        padding:50px 0;
    }
`;
const Block = styled.div`
    width:100%;
    border-radius:12px;
    background:#fff;
`;
const Currency = styled(Block)`
    padding-left:50px;
    max-width:660px;
    margin-right:15px;
    @media ${device.tablet}{
        max-width:100%;
        margin-left:0;
        margin-bottom:15px;
        padding-left:0px;
    }
`;
const Weather = styled(Block)`
    padding:35px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-direction:column;
    @media ${device.desktopM}{
        width:60%;
    }
    @media ${device.tablet}{
        width:100%;
    }
    @media ${device.tablet}{
        padding:20px;
    }
`;
const Head = styled.div`
    width:100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
`;
const Info = styled.div`
    display: flex;
    justify-content: center;
    @media ${device.mobile}{
        align-items:center;
    }
`;
const Icon = styled.img`
    width:55px;
    height:55px;
    margin-right:30px;
    @media ${device.mobile}{
        width:35px;
        height:35px;
        margin-right:15px;
    }
`;
const Overlay = styled.span`
    font:12px semibold;
    color:rgba(79,79,79,0.43);
`;
const Location = styled.p`
    font:26px semibold;
    color:#757575;
    @media ${device.mobile}{
        font-size:18px;
        padding-right:10px;
    }
`;
const Degree = styled.span`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    font:30px regular;
    color:#757575;
    @media ${device.mobile}{
        font-size:24px;
    }
    &:after{
        content:"";
        background:url(${celcius})no-repeat center;
        background-size:contain;
        width:18px;
        height:17px;
        margin-left:3px;
    }
`;
const Body = styled.div`
    width:100%;
    display: flex;
    justify-content:space-between;
    @media ${device.tablet}{
        margin-top:30px;
    }
    @media ${device.mobile}{
        overflow-x:auto;
        padding-bottom:10px;
    }
`;

const Daily = () => {
    const currencyArr = [
        {
            type:'USD',
            img:usdImg,
            value:'3.2523 GEL',
            convert:'3.2523 GEL'
        },
        {
            type:'EUR',
            img:eurImg,
            value:'3.0122GEL',
            convert:'3.0122GEL'
        },
        {
            type:'RUS',
            img:rusImg,
            value:'1.4523 GEL',
            convert:'1.4523 GEL'
        }
    ];
    const weatherArr = [
        {
            img:weatherNightIcon,
            date:'22 Nov'
        },
        {
            img:weatherNightIcon,
            date:'23 Nov'
        },
        {
            img:weatherNightIcon,
            date:'24 Nov'
        },
        {
            img:weatherNightIcon,
            date:'25 Nov'
        },
        {
            img:weatherNightIcon,
            date:'26 Nov'
        },
        {
            img:weatherNightIcon,
            date:'27 Nov'
        },
        {
            img:weatherNightIcon,
            date:'28 Nov'
        },
    ];
    return(
        <Section>
            <WrapperExt>
                <Currency>
                    {currencyArr.map(item => {
                        return <CurrencyBox
                            key={item.type+item.value}
                            type={item.type}
                            img={item.img}
                            value={item.value}
                            convert={item.convert}
                        />
                    })}
                </Currency>
                <Weather>
                    <Head>
                        <Info>
                            <Icon src={weatherNightIcon}/>
                            <div>
                                <Overlay>COUNTRY</Overlay>
                                <Location>Tbilisi, Georgia</Location>
                            </div>
                        </Info>
                        <Degree>23</Degree>
                    </Head>
                    <Body>
                        {weatherArr.map(item => {
                            return <WeatherBox key={item.date}
                                img={item.img}
                                date={item.date}
                            />
                        })}
                    </Body>
                </Weather>
            </WrapperExt>
        </Section>
    );
}

export default Daily;