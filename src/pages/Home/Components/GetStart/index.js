import styled from 'styled-components';
import Wrapper from '../../../../Components/Wrapper';
import ButtonArrow from '../../../../Components/ButtonArrow';
import bg from '../../img/car-bg.png';
import device from '../../../../config/device.js';

const WrapperExt = styled(Wrapper)`
    display:flex;
    justify-content:flex-end;
`;
const Section = styled.div`
    position:relative;
    width:100%;
    padding:250px 0;
    background:url(${bg}) no-repeat center;
    background-size:70%;
    background-position:-50px center;
    @media ${device.desktopM}{
        padding:100px 0;
    }
    @media ${device.mobile}{
        background:none;
        padding:50px 0;
    }
`;
const Info = styled.div`
    width:100%;
    max-width:580px;
    @media ${device.desktopM}{
        max-width:500px;
    }
    @media ${device.tablet}{
        max-width:380px;
    }
    @media ${device.mobile}{
        max-width:100%;
    }
`;
const Title = styled.h2`
    width:100%;
    font:28px semibold;
    line-height:44px;
    color:#4F4F4F;
    text-transform:uppercase;
    @media ${device.mobile}{
        font-size:20px;
        line-height:26px;
    }
`;
const Desc = styled.p`
    width:100%;
    font:14px medium;
    line-height:24px;
    color:#979998;
    margin:8px 0 40px 0;
`;
const GetStart = () => {
    return(
        <Section>
            <WrapperExt>
                <Info>
                    <Title>Get the best <br/> service in Georgia from Airport</Title>
                    <Desc>Pellentesque non tempor neque. Quisque ac purus nunc. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus dapibus sollicitudin vestibulum. Nunc rutrum urna non interdum finibus, leo magna dapibus lorem, sed mollis dolor orci at nisi.</Desc>
                    <ButtonArrow>GET STARTED</ButtonArrow>
                </Info>
            </WrapperExt>
        </Section>
    );
}

export default GetStart;