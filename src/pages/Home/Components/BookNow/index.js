import styled from 'styled-components';
import Wrapper from '../../../../Components/Wrapper';
import bg from '../../img/section1.jpg';
import btnArrowIcon from '../../img/arrow-btn.svg';
import device from '../../../../config/device.js';

const Section = styled.div`
    position:relative;
    width:100%;
    min-height:800px;
    height:100vh;
    background:url(${bg}) no-repeat center;
    background-size:cover;
    display: flex;
    justify-content: center;
    align-items: center;
    padding-bottom:200px;
    @media ${device.mobile}{
        padding-bottom: 180px;
        padding-top: 100px;
        min-height:100vh;
        height:auto;
    }
`;
const Text = styled.div`
    width:100%;
    max-width:1375px;
    position:relative;
    @media ${device.mobile}{
        padding-right:30px;
    }
`;
const Dot = styled.div`
    display: flex;
    flex-direction:column;
    position:absolute;
    top:50%;
    right:0;
    transform:translate(0%,-50%);
`;
const DotItemExt = styled.div`
    width:8px;
    height:8px;
    border-radius:50%;
    background:rgba(255,255,255,0.4);
    margin:7.5px 0;
`;
const DotItem = styled(DotItemExt)(props => ({
    background: props?.background,
}));
const Title = styled.h1`
    font:34px semibold;
    color:#fff;
    line-height:44px;
    text-transform:uppercase;
    margin-bottom:15px;
    @media ${device.mobile}{
        font-size:20px;
        line-height:24px;
    }
`;
const Desc = styled.p`
    font:14px medium;
    color:#fff;
    line-height:24px;
    width:100%;
    max-width:720px;
    @media ${device.mobile}{
        font-size:12px;
        line-height:16px;
    }
`;
const Action = styled.div`
    position:absolute;
    bottom:200px;
    left:0;
    width:90vw;
    height:135px;
    border-radius:0 10px 10px 0;
    background:#fff;
    display: flex;
    justify-content: space-between;
    align-items: stretch;
    overflow:hidden;
    @media ${device.desktopM}{
        width:100%;
        border-radius:0;
    }
    @media ${device.tablet}{
        height:100px;
    }
    @media ${device.mobile}{
        height:auto;
        flex-direction:column;
        bottom:0px;
    }
`;
const ActionWrapper = styled.div`
    width:100%;
    display: flex;
    justify-content: center;
    align-items: stretch;
    @media ${device.mobile}{
        flex-wrap:wrap;
    }
`;
const ActionItem = styled.div`
    width:25%;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction:column;
    padding-left:30px;
    position:relative;
    &:before{
        position:absolute;
        right:0;
        top:50%;
        transform:translate(0,-50%);
        width:1px;
        height:60px;
        background:#D8D8D8;
        content:"";
        @media ${device.tablet}{
            height:40px;
        }
    }
    &:last-child{
        &:before{
            display:none;
        }
    }
    @media ${device.tablet}{
        padding-left:0;
    }
    @media ${device.mobile}{
        width:50%;
        padding:10px 0;
        &:nth-child(2n){
            &:before{
                display: none;
            }
        }
    }
`;
const Overlay = styled.div`
    font:12px semibold;
    color:#4f4f4f;
    text-transform:uppercase;
    @media ${device.tablet}{
        font-size:11px;
    }
`;
const Button = styled.div`
    width:220px;
    background:#15A748;
    flex-shrink:0;
    cursor:pointer;
    display: flex;
    justify-content: center;
    align-items: center;
    font:20px semibold;
    color:#fff;
    line-height:22px;
    @media ${device.desktopM}{
        width:180px;
    }
    @media ${device.tablet}{
        width:130px;
        font-size:16px;
        line-height:18px;
    }
    @media ${device.mobile}{
        width:100%;
        height:50px;
        br{
            content:"";
        }
    }
    &:after{
        background:url(${btnArrowIcon})no-repeat center;
        background-size:contain;
        width:10px;
        height:14px;
        margin-left:12px;
        content:"";
    }
`;
const Input =  styled.input.attrs({
    type: "text",
})`
    border:none;
    outline:none;
    font:16px regular;
    color:#4F4F4F;
    width:100%;
    max-width:170px;
    @media ${device.tablet}{
        max-width:110px;
        font-size:12px;
    }
    &:placeholder{
        font:16px regular;
        color:#CCCBCB;
        @media ${device.tablet}{
            font-size:12px;
        }
    }
`;
const InputNum =  styled.input.attrs({
    type: "number",
})`
    width:100%;
    max-width:170px;
    border:none;
    outline:none;
    padding-right:10px;
    font:16px regular;
    color:#4F4F4F;
    @media ${device.tablet}{
        max-width:110px;
        font-size:12px;
    }
    &:placeholder{
        font:16px regular;
        color:#CCCBCB;
        @media ${device.tablet}{
            font-size:12px;
        }
    }
`
const BookNow = () => {
    return(
        <Section>
            <Wrapper>
                <Text>
                    <Title>Book any <br/> airport taxi in any direction</Title>
                    <Desc>Pellentesque non tempor neque. Quisque ac purus nunc. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus dapibus sollicitudin vestibulum. Nunc rutrum urna non interdum finibus, leo magna dapibus lorem, sed mollis dolor orci at nisi.</Desc>
                    <Dot>
                        <DotItem background="#fff"/>
                        <DotItem/>
                        <DotItem/>
                    </Dot>
                </Text>
            </Wrapper>
            <Action>
                <ActionWrapper>
                    <ActionItem>
                        <div>
                            <Overlay>Location</Overlay>
                            <Input placeholder={'Anywhere'}/>
                        </div>
                    </ActionItem>
                    <ActionItem>
                        <div>
                            <Overlay>Where</Overlay>
                            <Input placeholder={'Tbilis,Georgia'}/>
                        </div>
                    </ActionItem>
                    <ActionItem>
                        <div>
                            <Overlay>TRANSFER DATE</Overlay>
                            <InputNum placeholder={'DD / YY / MM'}/>
                        </div>
                    </ActionItem>
                    <ActionItem>
                        <div>
                            <Overlay>Adults</Overlay>
                            <InputNum min="0" placeholder={'Select  Adults'}/>
                        </div>
                    </ActionItem>
                </ActionWrapper>
                <Button>BOOK<br/>NOW</Button>
            </Action>
        </Section>
    );
}

export default BookNow;