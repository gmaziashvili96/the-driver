import styled from 'styled-components';
import device from '../../../../config/device.js';

const Item = styled.div`
    width:90px;
    height:120px;
    border-radius:6px;
    border:1px solid #E1E9E6;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction:column;
    margin-right:5px;
    &:last-child{
        margin-right:0;
    }
    @media ${device.tablet}{
        width:100%;
    }
    @media ${device.mobile}{
        max-width:90px;
        flex-shrink:0;
    }
`;
const Img = styled.img`
    width:34px;
    height:34px;
    object-fit:contain;
`;
const Temperature = styled.div`
    font:14px medium;
    color:#AAAAAA;
    margin-top:15px;
`;
const WeatherBox = (props) => {
    const {img,date} = props;
    return(
        <Item>
            <Img src={img}/>
            <Temperature>{date}</Temperature>
        </Item>
    );
}

export default WeatherBox;