
import styled from 'styled-components';
import arrowIcon from '../../img/slider-arrow-left.svg';
import playBG from '../../img/play-bg.jpg';
import playButtonIcon from '../../img/play.svg';
import device from '../../../../config/device.js';

const CurrencyItem = styled.div`
    height:90px;
    width:100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    border-bottom:1px solid #E1E9E6;
    padding-right:50px;
    &:last-child{
        border-bottom:none;
    }
    @media ${device.tablet}{
        padding:0 20px;
    }
    @media ${device.mobile}{
        height:70px;
    }
`;
const Info = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
`;
const Img = styled.img`
    width:44px;
    height:44px;
    object-fit:cover;
    border-radius:50%;
    margin-right:20px;
    @media ${device.mobile}{
        width:30px;
        height:30px;
        object-fit:cover;
        border-radius:50%;
        margin-right:10px;
    }
`;
const Detail = styled.div`
`;
const Type = styled.div`
    font:16px medium;
    color:#757575;
`;
const Equal = styled.div`
    font:12px regular;
    color:#C7C7C7;
`;
const Value = styled.div`
    font:20px medium;
    color:#757575;
    @media ${device.mobile}{
        font-size:14px;
    }
`;
const CurrencyBox = (props) => {
    const {img,type,value,convert} = props;
    return(
        <CurrencyItem>
            <Info>
                <Img src={img}/>
                <Detail>
                    <Type>{type}</Type>
                    <Equal>1 {type} = {convert}</Equal>
                </Detail>
            </Info>
            <Value>{value}</Value>
        </CurrencyItem>
    );
}

export default CurrencyBox;