import styled from 'styled-components';
import Wrapper from '../../../../Components/Wrapper';
import arrowIcon from '../../img/slider-arrow-left.svg';
import playBG from '../../img/play-bg.jpg';
import playButtonIcon from '../../img/play.svg';
import device from '../../../../config/device.js';

const WrapperExt = styled(Wrapper)`
    display:flex;
    justify-content:space-between;
    align-items:center;
    @media ${device.mobile}{
        flex-direction:column-reverse;   
    }
`;
const Section = styled.div`
    position:relative;
    width:100%;
    padding:100px 0;
    @media ${device.desktopM}{
        padding:50px 0;
    }
    &:after{
        position: absolute;
        top:0;
        left:0;
        width:75vw;
        height:100%;
        content:"";
        border-radius:0 60px 60px 0;
        background:#D2DAD8;
        z-index:-1;
        @media ${device.desktopM}{
            border-radius:0 30px 30px 0;
        }
        @media ${device.mobile}{
            border-radius:0;
            width:100%;
        }
    }
`;
const Slider = styled.div`
    max-width:720px;
`;
const SliderHd = styled.div`
    height:100px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    border-bottom:1px solid #C0C9C6;
`;
const SliderTitle = styled.div`
    width:100%;
    font:28px semibold;
    color:#4F4F4F;
    text-transform:uppercase;
    @media ${device.mobile}{
        font-size:20px;
    }
`;
const SliderBody = styled.div`
    
`;
const Arrows = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
`;
const Arrow = styled.div`
    cursor:pointer;
    width:50px;
    height:50px;
    background:url(${arrowIcon})no-repeat center;
    background-size:contain;
    margin-right:25px;
    &:last-child{
        margin-right:0;
        transform:rotate(180deg);
    }
    @media ${device.mobile}{
        margin-right:10px;
    }
`;
const Image = styled.div`
    width:650px;
    height:450px;
    min-width:325px;
    margin-left:25px;
    border-radius:20px;
    overflow:hidden;
    background:url(${playBG})no-repeat center;
    background-size:cover;
    display: flex;
    justify-content: center;
    align-items: center;
    @media ${device.mobile}{
        width:100%;
        height:200px;
        border-radius:5px;
        margin:0;   
        min-width:100%;
    }
`;
const PlayBtn = styled.button`
    width:100px;
    height:100px;
    border-radius:50%;
    background:url(${playButtonIcon})no-repeat center;
    background-color:#fff;
    border:none;
    outline:none;
    cursor:pointer;
    @media ${device.mobile}{
        width:60px;
        height:60px;
        background-size:20%;
    }
`;
const Overlay = styled.span`
    margin:24px 0 18px 0;
    display: block;
    font:14px medium;
    color:#A2A7A5;
`;
const Title = styled.h2`
    width:100%;
    font:22px semibold;
    line-height:29px;
    color:#4F4F4F;
    @media ${device.mobile}{
        font-size:18px;
        line-height:24px;
        margin-bottom:10px;
    }
`;
const Desc = styled.p`
    width:100%;
    font:14px medium;
    line-height:24px;
    color:#A4A4A4;
`;

const LatestNews = () => {
    return(
        <Section>
            <WrapperExt>
                <Slider>
                    <SliderHd>
                        <SliderTitle>Lastest news</SliderTitle>
                        <Arrows>
                            <Arrow/>
                            <Arrow/>
                        </Arrows>
                    </SliderHd>
                    <SliderBody>
                        <Overlay>3 November , 2019</Overlay>
                        <Title>Taxis have been regulated in Georgia, all taxis will be white from today.</Title>
                        <Desc>Pellentesque non tempor neque. Quisque ac purus nunc. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus dapibus sollicitudin vestibulum. Nunc rutrum urna non interdum finibus, leo magna dapibus lorem, sed mollis dolor orci at nisi.There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. </Desc>
                    </SliderBody>
                </Slider>
                <Image>
                    <PlayBtn/>
                </Image>
            </WrapperExt>
        </Section>
    );
}

export default LatestNews;