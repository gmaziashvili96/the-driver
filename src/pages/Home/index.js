import { Fragment } from "react";
import BookNow from "./Components/BookNow";
import Daily from "./Components/Daily";
import GetStart from "./Components/GetStart";
import LatestNews from "./Components/LatestNews";
import Footer from "../../Components/Footer";

const Home = () => {
    return(
        <Fragment>
            <BookNow/>
            <GetStart/>
            <LatestNews/>
            <Daily/>
            <Footer/>
        </Fragment>
    )
}

export default Home;