import React from 'react';
import styled from 'styled-components';
import Map from '../../Components/Map';
import {Link} from "react-router-dom";
import device from '../../config/device.js';
import MenuIcon from './img/menu.svg';
import Input from '../../Components/Input';
import Logo from '../../Components/Logo';
import carImg from './img/car.png';
import arrowIcon from './img/arrow.svg';


const Section = styled.div`
    display:flex;
    @media ${device.mobile}{
        flex-direction:column;
    }
`;
const Sidebar = styled.div`
    width:100%;
    max-width:545px;
    background:#fff;
    height:100vh;
    padding:40px 60px 0 60px;
    @media ${device.desktopM}{
        padding:20px 25px 0 25px;
        max-width:400px;
    }
    @media ${device.tablet}{
        padding:10px 15px 0 15px;
        max-width:300px;
    }
    @media ${device.mobile}{
        max-width:100%;
    }
`;
const MapBody = styled.div`
    width:100%;
    height:100vh;
    @media ${device.mobile}{
        height:50vh;
    }
`;
const Head = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom:70px;
`;
const Menu = styled.div`
    cursor: pointer;
    width:32px;
    height:20px;
    background:url(${MenuIcon})no-repeat center;
    background-size:contain;
`;
const Body = styled.div`
    overflow-y:auto;
    overflow-x:hidden;
    padding-right:5px;
    max-height:calc(100vh - 160px);
    &::-webkit-scrollbar {
        width:5px;
        border-radius:4px;
    }
    &::-webkit-scrollbar-track {
        background:#D4D4D4;
        border-radius:4px;
    }
    &::-webkit-scrollbar-thumb {
        border-radius:4px;
        background:#bfbfbf;
    }
`;
const Title = styled.h4`
    font:26px medium;
    color:#808080;
`;
const Overlay = styled.p`
    font:12px medium;
    color:#D4D4D4;
    margin-bottom:50px;
`;
const Fields = styled.div`
    &>div{
        margin-bottom:30px;
        max-width:100%;
    }
`;
const Slider = styled.div`
    width:100%;
`;
const SliderHD = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width:100%;
    margin-bottom:20px;
`;
const SliderTitle = styled.div`
    font:15px semibold;
    color:#616161;
    text-transform:uppercase;
`;
const SliderArrows = styled.div`
    display: flex;
`;
const SliderArrow = styled.div`
    width:8px;
    height:12px;
    background:url(${arrowIcon})no-repeat center;
    background-size:contain;
    transform:${props=>props.transform ||''};
    cursor: pointer;
    &:first-child{
        margin-right:24px;
    }
`;
const SliderItem = styled.div`
    width:200px;
    height:100px;
    border-radius:6px;
    border:1px solid #D1DAD7;
    position:relative;
    flex-shrink:0;
    margin-right:15px;
    padding:15px 0 0 15px;
    &:last-child{
        margin-right:0;
    }
`;
const SliderText = styled.div`
    font:14px semibold;
    color:#CACACA;
`;
const SliderRadioBox = styled.div`
    width:16px;
    height:16px;
    border-radius:50%;
    border:2px solid #CBCBCB;
    margin-right:5px;
`;
const SliderImg = styled.img`
    position:absolute;
    bottom:0;
    right:0;
    width:50%;
    height:80%;
    object-fit:contain;
    object-position:right;
`;
const SliderPlace = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
`;
const SliderBody = styled.div`
    width:calc(100% + 60px);
    display: flex;
    overflow-x:auto;
    &::-webkit-scrollbar {
        opacity:0;
    }
    &::-webkit-scrollbar-track {
        opacity:0;
    }
    &::-webkit-scrollbar-thumb {
        opacity:0;
    }
`;

const OrderPage = () => {
    return(
        <Section>
            <MapBody>
                <Map/>
            </MapBody>
            <Sidebar>
                <Head>
                    <Link to="/"><Logo/></Link>
                    <Menu/>
                </Head>
                <Body>
                    <Title>Order Taxi</Title>
                    <Overlay>Do you wont order taxi , please check all field. </Overlay>
                    <Fields>
                        <Input value="Tbilisi,Georgia" type="text" placeholder="Location"/>
                        <Input type="text" placeholder="Where you want"/>
                        <Input type="text" placeholder="Transfer Date"/>
                        <Input value="Three baby chair" type="text" placeholder="Baby Chair"/>
                        <Input value="3 Adults, 2 Children" type="text" placeholder="Adults"/>
                    </Fields>
                    <Slider>
                        <SliderHD>
                            <SliderTitle>Choose TAXI</SliderTitle>
                            <SliderArrows>
                                <SliderArrow transform={'rotate(180deg)'}></SliderArrow>
                                <SliderArrow></SliderArrow>
                            </SliderArrows>
                        </SliderHD>
                        <SliderBody>
                            <SliderItem>
                                <SliderPlace>
                                    <SliderRadioBox/>
                                    <SliderText>Standart</SliderText>
                                </SliderPlace>
                                <SliderImg src={carImg}/>
                            </SliderItem>
                            <SliderItem>
                                <SliderPlace>
                                    <SliderRadioBox/>
                                    <SliderText>Standart</SliderText>
                                </SliderPlace>
                                <SliderImg src={carImg}/>
                            </SliderItem>
                            <SliderItem>
                                <SliderPlace>
                                    <SliderRadioBox/>
                                    <SliderText>Standart</SliderText>
                                </SliderPlace>
                                <SliderImg src={carImg}/>
                            </SliderItem>
                        </SliderBody>
                    </Slider>
                    <Slider>
                        <SliderHD>
                            <SliderTitle>Choose TAXI</SliderTitle>
                            <SliderArrows>
                                <SliderArrow transform={'rotate(180deg)'}></SliderArrow>
                                <SliderArrow></SliderArrow>
                            </SliderArrows>
                        </SliderHD>
                        <SliderBody>
                            <SliderItem>
                                <SliderPlace>
                                    <SliderRadioBox/>
                                    <SliderText>Standart</SliderText>
                                </SliderPlace>
                                <SliderImg src={carImg}/>
                            </SliderItem>
                            <SliderItem>
                                <SliderPlace>
                                    <SliderRadioBox/>
                                    <SliderText>Standart</SliderText>
                                </SliderPlace>
                                <SliderImg src={carImg}/>
                            </SliderItem>
                            <SliderItem>
                                <SliderPlace>
                                    <SliderRadioBox/>
                                    <SliderText>Standart</SliderText>
                                </SliderPlace>
                                <SliderImg src={carImg}/>
                            </SliderItem>
                        </SliderBody>
                    </Slider>
                </Body>
            </Sidebar>
        </Section>
    )
}

export default OrderPage;