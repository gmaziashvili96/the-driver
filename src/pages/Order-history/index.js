import React from 'react';
import styled from 'styled-components';
import Wrapper from './../../Components/Wrapper';
import device from '../../config/device.js';
import Footer from '../../Components/Footer/';
import ProfileView from '../../Components/ProfileView/';
import profilePic from './img/profile-pic.png';

import Order from './Components/Order';

const Main = styled.div`
    padding:195px 0 110px 0;
    display: flex;
    justify-content: space-between;
    @media ${device.desktopM}{
        flex-direction:column;
        padding:150px 0 100px 0;
    }
`;
const Action = styled.div`
    width:100%;
    background:#fff;
    border-radius:10px;
    padding:35px 50px;
    margin-left:10px;
    @media ${device.desktopM}{
        margin-left:0px;
        margin-top:10px;
    }
    @media ${device.tablet}{
        padding:25px;
    }
`;
const ActionProfile = styled.div`
    width:100%;
    display:flex;
    flex-wrap:wrap;
    justify-content:space-between;
    margin-bottom:30px;
    &>div{
        margin-top:25px;
        width:100%;
        max-width:32.5%;
    }
`;
const Title = styled.h4`
    font:18px medium;
    color:#575757;
`;
const Desc = styled.p`
    font:11px medium;
    color:#C0C0C0;
    margin-bottom:36px;
`;


const OrderHistory = () => {
    const user = {
        name:'Avtandil Vardosanidze',
        location:'Tbilisi,Georgia',
        avatar:profilePic,
    }
    return(
        <React.Fragment>
            <Wrapper>
                <Main>
                    <ProfileView 
                        name={user.name} 
                        location={user.location} 
                        avatar={user.avatar}/>
                    <Action>
                        <Title>Profile Settings</Title>
                        <Desc>Personal information about your profile </Desc>
                        <Order/>
                        <Order/>
                        <Order/>
                        <Order/>
                        <Order/>
                    </Action>
                </Main>
            </Wrapper>
            <Footer/>
        </React.Fragment>
    )
}

export default OrderHistory;