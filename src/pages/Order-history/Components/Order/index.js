import styled from 'styled-components';
import arrowIcon from '../../img/arrow.svg';
import QRIcon from '../../img/QR.svg';
import React from 'react';
import device from '../../../../config/device';

const Main = styled.div`
    width:100%;
    border-radius:6px;
    border:1px solid #D1DAD7;
    margin-bottom:14px;
    &:last-child{
        margin-bottom:0;
    }
`;

const Item = styled.div`
    width:100%;
    display: flex;
    justify-content: center;
    align-items: flex-start;
    flex-direction:column;
    padding:0 5px;
    &:first-child{
        width:65%;
        @media ${device.mobile}{
            width:100%;
        }
    }
    &:nth-child(4){
        width:75%;
        padding-right:70px;
        @media ${device.mobile}{
            width:100%;
            padding-right:5px;
        }
    }
    @media ${device.mobile}{
        width:100%;
        padding-bottom:15px;
    }
`;
const FirstView = styled.div`
    display: flex;
    height:70px;
    padding:0 0 0 11px;
    position:relative;
    cursor: pointer;
    @media ${device.mobile}{
        flex-wrap:wrap;
        height:auto;
        padding:15px 0;
        padding-bottom:35px;
    }
`;
const Body = styled.div`
    display: flex;
    height:70px;
    padding:0 0 0 11px;
    border-top:1px solid #D4DCDA;
    @media ${device.mobile}{
        flex-wrap:wrap;
        height:auto;
        padding:15px 0;
    }
`;
const SecondView = styled.div`
    
`;
const Overlay = styled.span`
    font:12px medium;
    color:#BFC8C5;
`;
const Text = styled.p`
    font-size:14px;
    color: ${props => props.color ? props.color : "#575757"};
    font-family: ${props => props.fontFamily ? props.fontFamily : "medium"};
`;
const ArrowIcon = styled.img`
    position:absolute;
    right:28px;
    width:13px;
    height:7px;
    object-fit:contain;
    top:50%;
    transform: ${props => props.rotate ? props.rotate + ' translate(-50%,0)' : ""};
    @media ${device.mobile}{
        right:28px;
        object-fit:contain;
        top:auto;
        bottom:20px;
        left:50%;
        transform: ${props => props.rotate ? props.rotate + ' translate(0,-50%)' : ""};
    }
`;
const ButtonPlace = styled.div`
    display: flex;
    padding:0 16px 16px 16px;
    @media ${device.mobile}{
        flex-direction:column;
    }
`;
const ButtonCommon = styled.button`
    display: flex;
    justify-content: center;
    align-items: center;
    cursor:pointer;
    outline:none;
    border:none;
    border-radius:4px;
    font-size:13px;
`;

const GetQrBtm = styled(ButtonCommon)`
    font-family:semibold;
    text-transform:uppercase;
    background-color:#15A748;
    color:#fff;
    padding:0 20px;
    margin-right:6px;
    &:before{
        width:20px;
        height:20px;
        background:url(${QRIcon})no-repeat center;
        background-size: contain;
        content:'';
        margin-right:10px;
    }
    @media ${device.mobile}{
        margin-right:0;
        padding:0;
        width:100%;
        height:36px;
        background-position:111px center;
    }
`;
const CancelOrderBtn = styled(ButtonCommon)`
    padding:0 30px;
    height:36px;
    border:1px solid rgba(217,31,68,0.4);
    background:transparent;
    font-family: medium;
    color:#D91F44;
    @media ${device.mobile}{
        flex-direction:column;
        width:100%;
        margin-top:15px;
        padding:0;
    }
`;

const Order = () => {
    const [isOpen, setOpen] = React.useState(false);
    return(
        <Main>
            <FirstView onClick={()=>setOpen(!isOpen)}>
                <Item>
                    <Overlay>DATE & TIME</Overlay>
                    <Text>25 Mar 2019   22:30</Text>
                </Item>
                <Item>
                    <Overlay>LOCATION</Overlay>
                    <Text>Tbilisi International Aeroport </Text>
                </Item>
                <Item>
                    <Overlay>CAR MODEL</Overlay>
                    <Text>Merceders - Benz S class 2019</Text>
                </Item>
                <Item>
                    <Overlay>TOTAL PRICE</Overlay>
                    <Text color='#15A748' fontFamily="semibold">55.60 GEL</Text>
                </Item>
                <ArrowIcon rotate={isOpen?'rotate(180deg)':null}src={arrowIcon}/>
            </FirstView>
            {isOpen?
                <SecondView>
                    <Body>
                        <Item>
                            <Overlay>ORDER ID</Overlay>
                            <Text>12943224455</Text>
                        </Item>
                        <Item>
                            <Overlay>CITY LOCATION</Overlay>
                            <Text>Irakli Abashidze street N33/34</Text>
                        </Item>
                        <Item>
                            <Overlay>Luggage</Overlay>
                            <Text>3x maximum</Text>
                        </Item>
                        <Item>
                            <Overlay>ADULTS</Overlay>
                            <Text fontFamily="semibold">5 Adult , 2 baby chair</Text>
                        </Item>
                    </Body>
                    <ButtonPlace>
                        <GetQrBtm>GET QR Code</GetQrBtm>
                        <CancelOrderBtn>Cancle order</CancelOrderBtn>
                    </ButtonPlace>
                </SecondView>:null}
        </Main>
    )
}

export default Order;