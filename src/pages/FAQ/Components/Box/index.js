import styled from 'styled-components';
import device from '../../../../config/device.js';

const Title = styled.h2`
    font:24px medium;
    color:#808080;
    line-height:35px;
    margin-bottom:14px;
    @media ${device.mobile}{
        font-size:19px;
        line-height:23px;
        margin-bottom:10px;
    }
`;

const Desc = styled.div`
    font:14px medium;
    color:#B6B6B6;
    line-height:24px;
`;

const Main = styled.div`
    cursor: pointer;
    border-radius:10px;
    border:1px solid #C4CECB;
    padding:30px;
    min-height:344px;
    transition:0.3s all;
    &:hover{
        background:#fff;
    }
    @media ${device.mobile}{
        min-height:auto;
        padding:20px;
    }
`;

const Box = (props) => {
    const {title,desc} = props;
    return(
        <Main>
            <Title>{title}</Title>
            <Desc>{desc}</Desc>
        </Main>
    )
}

export default Box;