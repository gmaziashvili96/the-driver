import React from 'react';
import styled from 'styled-components';
import Wrapper from './../../Components/Wrapper';
import Box from "./Components/Box";
import device from '../../config/device.js';
import Footer from '../../Components/Footer';

const Main = styled.div`
    padding:195px 0 110px 0;
    width:100%;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-column-gap:12px;
    grid-row-gap:24px;
    @media ${device.desktopM}{
        padding:130px 0;
        grid-template-columns: 1fr 1fr 1fr;
    }
    @media ${device.tablet}{
        grid-template-columns: 1fr 1fr;
    }
    @media ${device.mobile}{
        grid-template-columns: 1fr;
    }
`;


const AboutUs = () => {
    const infoArr = [
        {
            title:'How do I cancel my homes reservation?1',
            desc:"Whether the reservation is two days or two months away, we hold the payment until 24 hours after check-in before giving it to the host. This hold gives both parties time to make sure that everything is as expected.",
        },
        {
            title:'How do I cancel my homes reservation?2',
            desc:"Whether the reservation is two days or two months away, we hold the payment until 24 hours after check-in before giving it to the host. This hold gives both parties time to make sure that everything is as expected.",
        },
        {
            title:'How do I cancel my homes reservation?3',
            desc:"Whether the reservation is two days or two months away, we hold the payment until 24 hours after check-in before giving it to the host. This hold gives both parties time to make sure that everything is as expected.",
        },
        {
            title:'How do I cancel my homes reservation?4',
            desc:"Whether the reservation is two days or two months away, we hold the payment until 24 hours after check-in before giving it to the host. This hold gives both parties time to make sure that everything is as expected.",
        },
        {
            title:'How do I cancel my homes reservation?5',
            desc:"Whether the reservation is two days or two months away, we hold the payment until 24 hours after check-in before giving it to the host. This hold gives both parties time to make sure that everything is as expected.",
        },
        {
            title:'How do I cancel my homes reservation?6',
            desc:"Whether the reservation is two days or two months away, we hold the payment until 24 hours after check-in before giving it to the host. This hold gives both parties time to make sure that everything is as expected.",
        },
        {
            title:'How do I cancel my homes reservation?7',
            desc:"Whether the reservation is two days or two months away, we hold the payment until 24 hours after check-in before giving it to the host. This hold gives both parties time to make sure that everything is as expected.",
        },
        {
            title:'How do I cancel my homes reservation?8',
            desc:"Whether the reservation is two days or two months away, we hold the payment until 24 hours after check-in before giving it to the host. This hold gives both parties time to make sure that everything is as expected.",
        },
    ]
    return(
        <React.Fragment>
            <Wrapper>
                <Main>
                    {infoArr.map( item => {
                        return <Box key={item.title+=1}
                            title={item.title}
                            desc={item.desc}
                        />
                    })}
                </Main>
            </Wrapper>
            <Footer/>
        </React.Fragment>
    )
}

export default AboutUs;