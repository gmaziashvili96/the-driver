import React from 'react';
import styled from 'styled-components';
import Wrapper from './../../Components/Wrapper';
import device from '../../config/device.js';
import Footer from '../../Components/Footer/';
import ProfileView from '../../Components/ProfileView/';
import profilePic from './img/profile-pic.png';
import Input from '../../Components/Input';
import Button from '../../Components/Button';

const Main = styled.div`
    padding:195px 0 110px 0;
    display: flex;
    justify-content: space-between;
    @media ${device.desktopM}{
        flex-direction:column;
        padding:150px 0 100px 0;
    }
`;
const Action = styled.div`
    width:100%;
    background:#fff;
    border-radius:10px;
    padding:35px 50px;
    margin-left:10px;
    @media ${device.desktopM}{
        margin-left:0;
        margin-top:10px;
    }
    @media ${device.mobile}{
        flex-direction:column;
        min-width:auto;
        padding:25px;
    }
`;
const ActionProfile = styled.div`
    width:100%;
    display:flex;
    flex-wrap:wrap;
    justify-content:space-between;
    margin-bottom:30px;
    &>div{
        margin-top:25px;
        width:100%;
        max-width:32.5%;
        @media ${device.mobile}{
            max-width:100%;
        }
    }
    @media ${device.mobile}{
        flex-direction:column;
    }
`;
const Title = styled.h4`
    font:18px medium;
    color:#575757;
`;
const Desc = styled.p`
    font:11px medium;
    color:#C0C0C0;
`;
const ButtonBody = styled.div`
    width:100%;
    display: flex;
    justify-content:flex-end;
    &>button:first-child{
        margin-right:10px;
        @media ${device.mobile}{
            margin-right:0;
            margin-top:10px;
        }
    }
    @media ${device.mobile}{
        flex-direction:column-reverse;
    }
`;

const Settings = () => {
    const user = {
        name:'Avtandil Vardosanidze',
        location:'Tbilisi,Georgia',
        avatar:profilePic,
    }
    return(
        <React.Fragment>
            <Wrapper>
                <Main>
                    <ProfileView 
                        name={user.name} 
                        location={user.location} 
                        avatar={user.avatar}/>
                    <Action>
                        <Title>Profile Settings</Title>
                        <Desc>Personal information about your profile </Desc>
                        <ActionProfile>
                            <Input type='text' value="Avto Vardosanidze" placeholder='First name'/>
                            <Input type='text' value="Tbilis,Georgia" placeholder='Country'/>
                            <Input type='text' value="Mikheil Shashishvili Street N5" placeholder='Adress'/>
                            <Input type='text' value="+995 595 04 56 61" placeholder='Phone number'/>
                            <Input type='text' value="Flytaxi@gmail.com" placeholder='E-mail Adress'/>
                            <Input type='text' value="12 December, 1998" placeholder='Date'/>
                        </ActionProfile>
                        <Title>Personal informations</Title>
                        <Desc>Change password</Desc>
                        <ActionProfile>
                            <Input value="some password" type='password' placeholder='Old Password'/>
                            <Input type='password' placeholder='New Password'/>
                            <Input type='password' placeholder='Repeat Password'/>
                        </ActionProfile>
                        <ButtonBody>
                            <Button 
                                border='1px solid #D1DAD7;' 
                                color='#C5CDCB'
                                background='transparent' >Clear</Button>
                            <Button>Save changes</Button>
                        </ButtonBody>
                    </Action>
                </Main>
            </Wrapper>
            <Footer/>
        </React.Fragment>
    )
}

export default Settings;