import React from 'react';
import styled from 'styled-components';
import Wrapper from './../../Components/Wrapper';
import Map from "./../../Components/Map";
import background from './img/background.png';
import device from '../../config/device.js';
import Footer from '../../Components/Footer';

const Section = styled.div`
    background:url(${background})no-repeat;
    background-position:bottom center;
    background-size:cover;
    padding:220px 0 140px 0;
    @media ${device.desktopM}{
        padding:150px 0;
    }
    @media ${device.tablet}{
        padding:150px 0 100px 0;
    }
    @media ${device.mobile}{
        padding:100px 0 50px 0;
    }
`;
const WrapperExt = styled(Wrapper)`
    display: flex;
    justify-content: space-between;
    align-items: stretch;
    @media ${device.tablet}{
        flex-direction:column-reverse;
    }
`;
const MapContainer = styled.div`
    width:100%;
    max-width:800px;
    height:520px;
    border-radius:16px;
    overflow:hidden;
    box-shadow:0 4px 16px rgba(0,0,0,.3);
    @media ${device.tablet}{
        max-width:100%;
        height:300px;
        border-radius:10px;
        margin-top:30px;
    }
`;
const Info = styled.div`
    width:100%;
    padding-left:75px;
    display: flex;
    justify-content:center;
    flex-direction:column;
    @media ${device.desktopM}{
        padding-left:35px;
    }
    @media ${device.mobile}{
        padding-left:0px;
    }
`;
const Title = styled.h2`
    font:30px semibold;
    color:#575757;
    margin-bottom:10px;
    @media ${device.mobile}{
        font-size:24px;
    }
`;
const Desc = styled.p`
    font:14px medium;
    color:#9A9A9A;
    line-height:22px;
    @media ${device.mobile}{
        font-size:12px;
        line-height:18px;
    }
`;
const Contact = styled.div`
    margin-top:35px;
    width:100%;
    display: flex;
    justify-content: space-between;
    flex-wrap:wrap;
    @media ${device.mobile}{
        margin-top:15px;
    }
`;
const Item = styled.div`
    font:18px medium;
    color:#575757;
    width:40%;
    margin-top:20px;
    @media ${device.desktopM}{
        width:50%;
    }
    @media ${device.mobile}{
        width:100%;
        font-size:16px;
        margin-top:10px;
    }
`;
const AboutUs = () => {
    return(
        <React.Fragment>
            <Section>
                <WrapperExt>
                    <MapContainer>
                        <Map/>
                    </MapContainer>
                    <Info>
                        <div>
                            <Title>CONTACT US !</Title>
                            <Desc>With technologies never before offered on a production car, the E-Class writes a new chapter in the story of driving: Where cars can talk to each other, and look out for you in ways you never imagined.</Desc>
                        </div>
                        <Contact>
                            <Item>0160 Tbilisi, Georgia</Item>
                            <Item>T (+995) 555 54 30 57</Item>
                            <Item>Nutsubidze str #129A</Item>
                            <Item>Infoonline@flaytaxi.ge</Item>
                        </Contact>
                    </Info>
                </WrapperExt>
            </Section>
            <Footer/>
        </React.Fragment>
    )
}

export default AboutUs;