import styled from 'styled-components';
import Wrapper from './../../Components/Wrapper';
import Slider from "./Components/Slider";
import carBg from './img/car-bg.png';
import ButtonArrow from './../../Components/ButtonArrow';
import luggageIcon from './img/luggage.svg';
import motIcon from './img/mot.svg';
import device from './../../config/device.js';

const Section = styled.div`
    background:url(${carBg})no-repeat;
    background-position:47vw center;
    background-size:60%;
    padding:220px 0 100px 0;
    min-height:100vh;
    @media ${device.tablet}{
        background-position:center 100px;
        background-size:auto 350px;
        padding-top:400px;
    }
   
`;
const WrapperExt = styled(Wrapper)`
    display: flex;
    justify-content: space-between;
    align-items: stretch;
    padding-top:180px;
    @media ${device.desktopM}{
        padding-top:0px;
    }
    @media ${device.tablet}{
        flex-direction:column;
        padding-top:0;
    }
`;
const Item = styled.h2`
    width:100%;
    display: flex;
    align-items:flex-end;
    @media ${device.tablet}{
        width:100%;
    }
`;
const Reserve = styled.div`
    display: flex;
    margin-left:auto;
    @media ${device.tablet}{
        margin-top:40px;
        width:100%;
    }
    @media ${device.mobile}{
        flex-wrap:wrap;
        justify-content:space-between;
    }
`;
const Details = styled.div`
    width:150px;
    height:65px;
    border-radius:5.5px;
    border:1px solid #B9C2BF;
    padding:12px 0 12px 20px;
    display: flex;
    align-items: center;
    margin-right:16px;
    @media ${device.tablet}{
        width:100%;
    }
    @media ${device.mobile}{
        margin-bottom:15px;
        width:49%;
        margin-right:0;
    }
`;
const Image = styled.img`
    width:auto;
    height:100%;
    object-fit:contain;
    margin-right:10px;
`;
const Count = styled.span`
    font:26px regular;
    color:#8E9897;
    line-height:20px;
`;
const Overlay = styled.p`
    font:11px semibold;
    color:#B6BCBA;
`;

const AboutUs = () => {
    return(
        <Section>
            <WrapperExt>
                <Item>
                    <Slider/>
                </Item>
                <Item>
                    <Reserve>
                        <Details>
                            <Image src={luggageIcon}></Image>
                            <div>
                                <Count>2x</Count>
                                <Overlay>MAXIMUM</Overlay>
                            </div>
                        </Details>
                        <Details>
                            <Image src={motIcon}></Image>
                            <div>
                                <Count>3x</Count>
                                <Overlay>MAXIMUM</Overlay>
                            </div>
                        </Details>
                        <ButtonArrow 
                            boxShadow={'0 10px 22px rgba(21,167,72,0.4);'} 
                            height={'65px'}> Reservation
                        </ButtonArrow>
                    </Reserve>
                </Item>
            </WrapperExt>
        </Section>
        
    )
}

export default AboutUs;