import styled from 'styled-components';
import arrowIcon from '../../img/arrow.svg';
import device from '../../../../config/device.js';

const Main = styled.div`
    width:100%;
    max-width:540px;
    @media ${device.desktopM}{
        padding-top:0px;
    }
    @media ${device.tablet}{
        max-width:100%;
    }
`;
const Container = styled.div`
    width:100%;
`;
const Item = styled.div``;
const Overlay = styled.span`
    font:24px medium;
    color:#A0A2A1;
    @media ${device.mobile}{
        font-size:16px;
    }
`;
const Title = styled.h3`
    font:44px medium;
    color:#575757;
    margin:20px 0 10px 0;
    @media ${device.mobile}{
        font-size:30px;
    }
`;
const Desc = styled.p`
    font:14px medium;
    color:#B6B6B6;
    line-height:24px;
    @media ${device.mobile}{
        font-size:12px;
    }
`;
const Navigation = styled.div`
    margin-top:180px;
    display: flex;
    align-items: center;
    @media ${device.tablet}{
        margin-top:30px;
    }
`;
const Arrow = styled.div`
    cursor: pointer;
    width:50px;
    height:50px;
    background:#fff;
    border-radius:50%;
    display: flex;
    justify-content: center;
    align-items: center;
    &:before{
        opacity:0.5;
        content:"";
        width:8px;
        height:13px;
        background:url(${arrowIcon})no-repeat;
        background-size:contain;
        transition:0.3s all;
    }
    &:first-child{
        &:before{
            transform:rotate(180deg);
        }
    }
    &:hover:before{
        opacity:1;
    }
`;

const Pagination = styled.div`
    margin:0 35px;
    font:20px regular;
    color:#A5A5A5;
    &>span{
        color:#575757;
    }
`;


const Slider = () => {
    return(
        <Main>
            <Container>
                <Item>
                    <Overlay>Businness Class</Overlay>
                    <Title>2019 Tesla Model 3 Standard</Title>
                    <Desc>With technologies never before offered on a production car, the E-Class writes a new  chapter in the story of driving: Where cars can talk to each other, and look out for you in ways you never imagined.</Desc>
                </Item>
            </Container>
            <Navigation>
                <Arrow/>
                <Pagination>
                    <span>01</span>  /  05
                </Pagination>
                <Arrow/>
            </Navigation>
        </Main>
        
    )
}

export default Slider;