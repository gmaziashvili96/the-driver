import styled from 'styled-components';

const Wrapper = styled.div`
    width:100%;
    max-width:1540px;
    padding:0 20px;
    margin:0 auto;
`;

export default Wrapper;