import styled from 'styled-components';
import React from 'react';
import settingArrowIcon from '../../assets/img/arrow-setting.svg';
import settingIcon from '../../assets/img/settings.svg';
import pfSettingIcon from '../../assets/img/setting-dark.svg';
import pfHistoryIcon from '../../assets/img/history.svg';
import pfLogoutIcon from '../../assets/img/logout.svg';
import device from '../../config/device';

const CommonStyle = styled.div`
    border-radius:10px;
    background:#fff;
    box-shadow:0 1px 4px rgba(0,0,0,0.1);
    width:100%;
`;
const Section = styled.div`
    min-width:320px;
    @media ${device.desktopM}{
        display:flex;
        flex-direction:row;
    }
    @media ${device.mobile}{
        flex-direction:column;
        min-width:auto;
    }
`;
const Main = styled(CommonStyle)`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction:column;
    padding:42px 0;
`;
const Avatar = styled.div`
    width:100px;
    height:100px;
    position:relative;
    margin-bottom:15px;
    @media ${device.desktopM}{
        display:flex;
    }
`;
const Note = styled.div`
    position:absolute;
    top:5px;
    right:-11px;
    border:3px solid #fff;
    background:#15A748;
    width:38px;
    height:24px;
    border-radius:12px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding:0 4px;
`;
const Setting = styled.img`
    height:12px;
    width:12px;
    object-fit:contain;
`;
const ArrowDown = styled.img`
    height:8px;
    width:6px;
    object-fit:contain;
`;
const Image = styled.img`
    width:100%;
    height:100%;
    border-radius:50%;
    object-fit:cover;
    object-position:center;
`;
const Location = styled.span`
    font:12px medium;
    color:#CACACA;
`;
const Name = styled.p`
   font:18px semibold;
   color:#575757;
`;

const Navigation = styled(CommonStyle)`
    margin-top:10px;
    @media ${device.desktopM}{
        margin:0;
        margin-left:10px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction:column;
    }
    @media ${device.mobile}{
        margin-left:0;
        margin-top:10px;
    }
`;
const Text = styled.div`
   border-bottom:1px solid #EEEEEE;
   width:100%;
   display: flex;
   justify-content: space-between;
   align-items: center;
`;
const Item = styled.div`
   width:100%;
   height:60px;
   display:flex;
   align-items:stretch;
   font:14px medium;
   color:#ABABAB;
   cursor:pointer;
   transition:0.3s all;
   &:hover{
       background:#F8F8F8;
   }
   &:last-child{
        ${Text}{
            border-bottom:none;
        }
   }
`;
const Icon = styled.div`
   width:60px;
   height:60px;
   display: flex;
   justify-content: center;
   align-items: center;
`;
const IconImg = styled.img`
   width:20px;
   height:20px;
   object-fit:contain;
`;
const Count = styled.div`
    width:40px;
    height:25px;
    border-radius:15px;
    display: flex;
    justify-content: center;
    align-items: center;
    font:12px semibold;
    color:#ABABAB;
    margin-right:25px;
    background:#EEEEEE;
`;
const ProfileView = (props) => {
    const {name,location,avatar} = props;
    return(
        <Section>
            <Main>
                <Avatar>
                    <Note>
                        <Setting src={settingIcon}/>
                        <ArrowDown src={settingArrowIcon}/>
                    </Note>
                    <Image  src={avatar}/>
                </Avatar>
                <Location>{location}</Location>
                <Name>{name}</Name>
            </Main>
            <Navigation>
                <Item>
                    <Icon>
                        <IconImg src={pfSettingIcon}/>
                    </Icon>
                    <Text>
                        <span>Profile settings</span>
                    </Text>
                </Item>
                <Item>
                    <Icon>
                        <IconImg src={pfHistoryIcon}/>
                    </Icon>
                    <Text>
                        <span>Order history</span>
                        <Count>24</Count>
                    </Text>
                </Item>
                <Item>
                    <Icon>
                        <IconImg src={pfLogoutIcon}/>
                    </Icon>
                    <Text>
                        <span>Log out</span>
                    </Text>
                </Item>
            </Navigation>
        </Section>
    );
}

export default ProfileView;