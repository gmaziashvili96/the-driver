import styled from 'styled-components';
import arrowDown from '../../assets/img/arrow-down.svg';
import en from '../../assets/img/en.jpg';
import device from '../../config/device';

const LangDiv = styled.div`
    cursor:pointer;
    width:75px;
    height:45px;
    border-radius:6px;
    border:1px solid #6F7677;
    display:flex;
    justify-content:space-between;
    align-items:stretch;
    @media ${device.tablet}{
        position:fixed;
        z-index:100;
        top:10px;
        right:10px;
    }
`;
const Flag = styled.div`
    width:70%;
    padding:10px;
`;
const FlagImg = styled.img`
    width:100%;
    height:100%;
    object-fit:cover;
`;
const Arrow = styled.div`
    width:30%;
    display:flex;
    justify-content:flex-start;
    align-items:center;
`;
const ArrowImg = styled.img`
    width:12px;
    height:8px;
    object-fit:contain;
`;

const Lang = () => {
    return(
        <LangDiv>
            <Flag>
                <FlagImg src={en}/>
            </Flag>
            <Arrow>
                <ArrowImg src={arrowDown}/>
            </Arrow>
        </LangDiv>
    )
}

export default Lang;