import styled from 'styled-components';
import logotypeWhite from '../../assets/img/logo-white.svg';
import logotypeBlack from '../../assets/img/logo-black.svg';
import device from '../../config/device';

const LogoDivMain = styled.div`
    width:80px;
    height:44px;
    background:url(${logotypeWhite}) no-repeat center;
    background-size:contain;
    @media ${device.mobile}{
        width:60px;
        height:24px;
    }
`;
const LogoDiv = styled(LogoDivMain)(props => ({
    background: props.type == 'white' ? 
    `url(${logotypeWhite}) no-repeat center` : 
    `url(${logotypeBlack}) no-repeat center`,
    backgroundSize:'contain'
}));

const Logo = (props) => {
    return <LogoDiv type={props.type}/>
}

export default Logo;