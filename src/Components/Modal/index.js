import styled from 'styled-components';
import React from 'react';
import closeIcon from '../../assets/img/close.svg';
const ModalBg = styled.div`
    position:fixed;
    top:0;
    left:0;
    width:100%;
    height:100%;
    background:rgba(0,0,0,0.4);
    z-index:9999;
`;
const Form = styled.div`
    padding:40px 50px 20px 50px ;
    border-radius:12px;
    background:#fff;
    position:fixed;
    top:50%;
    left:50%;
    transform:translate(-50%,-50%);
    z-index:99999;
    width:100%;
    max-width:${props=>props.maxWidth || '400px'};
`;
const FormAbs = styled.div`
    width:100%;
    position:relative;
`;
const Close = styled.div`
    cursor: pointer;
    width:18px;
    height:18px;
    background:url(${closeIcon})no-repeat center;
    background-size:contain;
    position:absolute;
    top:-20px;
    right:-30px;
`;
const Modal = (props) =>{
    return <React.Fragment>
        <ModalBg/>
        <Form  maxWidth={props.maxWidth}>
            <FormAbs>
                <Close/>
                {props.children}
            </FormAbs>
        </Form>
    </React.Fragment>
}
export default Modal;