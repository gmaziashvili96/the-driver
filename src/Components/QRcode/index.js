import styled from 'styled-components';
import React from 'react';
import QrcodeIcon from '../../assets/img/QRcode.png';
import downloadIcon from '../../assets/img/download.svg';

const Img = styled.img`
    width:100%;
    height:auto;
    margin-bottom:15px;
`;

const Button = styled.button`
    font:14px semibold;
    color:#fff;
    background:#15A748;
    height:46px;
    width:100%;
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
    outline:none;
    border:none;
    border-radius:4px;
    &:before{
        display: block;
        width:18px;
        height:18px;
        background:url(${downloadIcon})no-repeat center;
        background-size:contain;
        content:"";
        margin-right:14px;
    }
`;
const QRcode = () =>{
    return <React.Fragment>
        <Img src={QrcodeIcon}/>
        <Button>Download image</Button>
    </React.Fragment>
}
export default QRcode;