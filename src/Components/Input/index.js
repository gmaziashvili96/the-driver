import styled from 'styled-components';
import React from 'react';
import device from '../../config/device';

const Label = styled.div`
    position:relative;
    width:100%;
    max-width:330px;
    height:55px;
    font:14px medium;
    color:#575757;
    margin:${props=>props.margin || '0'};
    @media ${device.tablet}{
        height:40px;
        font-size:13px;
    }
`;
const Span = styled.span`
    position: absolute;
    font-size: inherit;
    font-family: inherit;;
    top: 50%;
    transform:translate(0,-50%);
    left:15px;
    cursor: text;
    transition: all 0.2s ease-in-out;
    color:#BFC8C5;
    padding:0 2px;
    z-index:0;
    @media ${device.tablet}{
        left:10px;
    }
`;
const InputValue = styled.input.attrs(props => ({
    required:true,
}))`
    padding:0 16px;
    font-size: inherit;
    font-family: inherit;
    border:1px solid #D1DAD7;
    outline:none;
    border-radius:4px;
    width:100%;
    height:100%;
    position:relative;
    z-index:1;
    background: transparent;
    @media ${device.tablet}{
        padding:0 10px;
    }
    &:focus ~ ${Span},
    &:valid ~ ${Span}{
        transform: translate(0, -220%);
        cursor: default;
        background:#fff;
        font-size:10px;
        z-index:2;
        @media ${device.tablet}{
            padding:0 10px;
            transform: translate(0, -170%);
        }
    }
`;
const Input = (props) => {
    const {type,value,placeholder,margin} = props;
    const [values, setValue] = React.useState(value);

    function onChange(e) {
        setValue(e.target.value)
    }

    return(
        <Label margin={margin}>
            <InputValue value={values} type={type} onChange={onChange}/>
            <Span>{placeholder}</Span>
        </Label>
    )
}

export default Input;