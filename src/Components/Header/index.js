import styled from 'styled-components';
import React from 'react';
import {Link,useLocation } from "react-router-dom";
import Wrapper from '../Wrapper';
import Logo from '../Logo';
import Lang from '../Lang';
import Button from '../Button';
import device,{size} from './../../config/device.js';
import menuIconWhite from '../../assets/img/menu-white.svg';
import menuIconBlack from '../../assets/img/menu-black.svg';
import Modal from '../../Components/Modal';
import SignIn from '../../Components/SignIn';

const HeaderDiv = styled.div`
  margin-bottom:-100px;
  width:100%;
  height:100px;
  display:flex;
  justify-content:space-between;
  align-items:center;
  position:relative;
  z-index:1;
`;

const Nav = styled.div`
  display:flex;
  align-items:center;
  @media ${device.tablet}{
    display:${props=>props.display};
    position:fixed;
    top:0;
    left:0;
    flex-direction:column;
    justify-content:center;
    background:#fff;
    height:100%;
    width:100%;
    z-index:99;
    &>div{
        font-size:30px;
        margin:0;
        margin-bottom:10px;
    }
  }
`;
const NavItemExt = styled.div`
  color:#fff;
  margin-right:60px;
  font-family:semibold;
`;
const NavItem = styled(NavItemExt)(props => ({
    color: props?.color,
}));

const MenuExt = styled.div`
    cursor:pointer;
    width: 36px;
    height: 20px;
    background:url(${menuIconBlack})no-repeat center;
    background-size:contain;
    display: none;
    @media ${device.tablet}{
        display: block;
    }
`;
const Menu = styled(MenuExt)(props => ({
    background: props.type === 'white' ? 
    `url(${menuIconWhite}) no-repeat center` : 
    `url(${menuIconBlack}) no-repeat center`
}));

const getWidth = () => window.innerWidth 
  || document.documentElement.clientWidth 
  || document.body.clientWidth;
  
function useCurrentWidth() {
    let [width, setWidth] = React.useState(getWidth());
  
    React.useEffect(() => {
      let timeoutId = null;
      const resizeListener = () => {
        clearTimeout(timeoutId);
        timeoutId = setTimeout(() => setWidth(getWidth()), 150);
      };
      window.addEventListener('resize', resizeListener);
      
      return () => {
        window.removeEventListener('resize', resizeListener);
      }
    }, [])
  
    return width;
}
const Header = () => {
    const [isOpenLogIn,setOpenLogIn] = React.useState();
    const [isOpenMenu,setOpenMenu] = React.useState(false);
    const location = useLocation();
    const windowWidth = useCurrentWidth();
    const currentPath = location.pathname;
    const themeColor = currentPath === '/' ? 'white' : 'black';
    const navThemeColor = windowWidth <= size.tablet ? 'black': themeColor;
    if(currentPath === '/order-page') return null;
    return(
        <React.Fragment>
            <Wrapper>
                <HeaderDiv>
                    <Link to="/"><Logo type={themeColor}/></Link>
                    <Nav display={isOpenMenu?'flex':'none'} onClick={
                        ()=>windowWidth <= size.tablet? setOpenMenu(false) : null
                    }>
                        <NavItem color={navThemeColor}>
                            <Link to="/About-us">Abou us</Link>
                        </NavItem>
                        <NavItem color={navThemeColor}>
                            <Link to="/Our-cars">Our Cars</Link>
                        </NavItem>
                        <NavItem color={navThemeColor}>
                            <Link to="/Contact-us">Contact</Link>
                        </NavItem>
                        <NavItem color={navThemeColor}>
                            <Link to="/FAQ">FAQ</Link>
                        </NavItem>
                        <Lang/>
                        <Button margin="0 12px">Reservation</Button>
                        <Button color='#15A748' background='#fff' onClick={()=>setOpenLogIn(!isOpenLogIn)}>SIGN IN</Button>
                    </Nav>
                    <Menu type={themeColor} onClick={()=>setOpenMenu(!isOpenMenu)}/>
                </HeaderDiv>
            </Wrapper>
            {isOpenLogIn ? 
                <Modal>
                    <SignIn/>
                </Modal>:
            null}
        </React.Fragment>
    )
}

export default Header;