import styled from 'styled-components';
import arrowRight from '../../assets/img/arrow-right.svg';
import device from './../../config/device.js';

const BtnMainStyle = styled.button`
    font:16px semibold;
    color:#fff;
    height:55px;
    padding:0 25px;
    cursor: pointer;
    outline:none;
    border:none;
    border-radius:6px;
    background-color:#15A748;
    display: flex;
    justify-content: center;
    align-items: center;
    @media ${device.mobile}{
        width:100%;
    }
    &:after {
        content:"";
        width:8px;
        height:14px;
        background:url(${arrowRight}) no-repeat;
        background-size:contain;
        margin-left:25px;
    }
`;
const Btn = styled(BtnMainStyle)(props => ({
    background: props?.background,
    color:props?.color,
    border:props?.border,
    margin:props?.margin,
    height:props?.height,
    boxShadow:props?.boxShadow,
}));

const ButtonArrow = (props) => {
    const {color,background,margin,boxShadow,height} = props;
    return <Btn color={color} 
            background={background} 
            margin={margin}
            height={height}
            boxShadow={boxShadow} >
        {props.children}
    </Btn>
}

export default ButtonArrow;