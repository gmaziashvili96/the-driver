import styled from 'styled-components';
import Wrapper from '../Wrapper';
import logoIcon from '../../assets/img/logo-black-white.svg';
import appleStoreIcon from '../../assets/img/appstore.png';
import googleStoreIcon from '../../assets/img/playstore.png';
import device from '../../config/device';

const Main = styled.div`
    background:#fff;
    padding:160px 0;
    width:100%;
    @media ${device.desktopM}{
        padding:100px 0;
    }
    @media ${device.mobile}{
        padding:50px 0;
    }
`;
const WrapperExt = styled(Wrapper)`
    display:flex;
    justify-content:space-between;
`;
const Logo = styled.div`
    width:200px;
    height:56px;
    background:url(${logoIcon})no-repeat center;
    background-size:contain;
    margin-bottom:55px;
    @media ${device.tablet}{
        margin-bottom:35px;
        width:150px;
        height:40px;
    }
`;
const Rights = styled.div`
    max-width:400px;
    @media ${device.mobile}{
        max-width:100%;
        width:100%;
    }
`;
const Span = styled.div`
    font:14px medium;
    color:#7E7E7E;
    margin-bottom:10px;
    @media ${device.mobile}{
        width:100%;
    }
`;
const Desc = styled.p`
    font:12px regular;
    color:#9D9D9D;
    margin-bottom:24px;
    @media ${device.mobile}{
        width:100%;
    }
`;
const ImageBody= styled.div`
    display:flex;
    &>img:first-child{
        margin-right:12px;
    }
`;
const Image = styled.img`
    width:130px;
    height:40px;
    object-fit:cover;
    object-position:center;
    @media ${device.mobile}{
        width:100px;
        height:30px;
    }
`;
const Info = styled.div`
    display: flex;
    justify-content:space-between;
    width:100%;
    max-width:900px;
    margin-left:20px;
    @media ${device.desktopM}{
        margin-left:0px;
    }
    @media ${device.tablet}{
        display:none;
    }
`;
const Column = styled.div`
    @media ${device.desktopM}{
        padding-left:20px;
    }
`;
const Title = styled.div`
    font:18px semibold;
    color:#393939;
    @media ${device.desktopM}{
        font-size:16px;
    }
`;
const Item = styled.div`
    font:16px medium;
    color:#A3A3A3;
    margin-top:28px;
    @media ${device.desktopM}{
        font-size:14px;
        margin-top:15px;
    }
`;

const Footer = () => {
    return(
        <Main>
            <WrapperExt>
                <Rights>
                    <Logo/>
                    <Span>Copyright 2019. All rights recived</Span>
                    <Desc>Mauris iaculis turpis nisl, facilisis ultricies mauris volutpat maximus. Phasellus tristique facilisis mauris, vel condimentum lectu</Desc>
                    <ImageBody>
                        <Image src={appleStoreIcon}/>
                        <Image src={googleStoreIcon}/>
                    </ImageBody>
                </Rights>
                <Info>
                    <Column>
                        <Title>Our Products</Title>
                        <Item>Scooters</Item>
                        <Item>Scooters Platform</Item>
                        <Item>Dispatch Software</Item>
                        <Item>Ride</Item>
                        <Item>Drive</Item>
                    </Column>
                    <Column>
                        <Title>Car models</Title>
                        <Item>Tesla car 3s</Item>
                        <Item>Merceders - benz vitto</Item>
                        <Item>Voltswagen</Item>
                        <Item>Toyota camry</Item>
                        <Item>Nissan R34 Couck</Item>
                    </Column>
                    <Column>
                        <Title>Contact Flytaxi</Title>
                        <Item>Contact@Flytaxi.com</Item>
                        <Item>(+995) 595 43 41 46</Item>
                        <Item>Tbilisi,Georgia</Item>
                        <Item>Alexander khazbegi Ave 12</Item>
                    </Column>
                </Info>
            </WrapperExt>
        </Main>
    );
}

export default Footer;