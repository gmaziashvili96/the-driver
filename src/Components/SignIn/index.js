import styled from 'styled-components';
import React from 'react';
import Input from '../Input';
import Button from '../Button';
const Title = styled.h2`
    font:20px semibold;
    color:#15A748;
    text-transform:uppercase;
    margin-bottom:40px;
`;
const Agree = styled.div`
    cursor: pointer;
    display: flex;
    align-items: center;
    margin-bottom:30px;
`;
const CheckBox = styled.div`
    width:15px;
    height:15px;
    border-radius:4px;
    border:1px solid #D1DAD7;
    margin-right:8px;
    display:flex;
    padding:3px;
    &:before{
        width:100%;
        height:100%;
        background:#D1DAD7;
        content:"";
    }
`;
const Overlay = styled.span`
    font:10px regular;
    color:#BFC8C5;
`;
const Span = styled.span`
    font:10px regular;
    color:#15A748;
    margin-left:auto;
`;
const SignIn = () =>{
    return <React.Fragment>
        <Title>Sign in</Title>
        <Input placeholder="E-mail Adress" margin='0 0 15px 0'/>
        <Input placeholder="Password" type="password" margin='0 0 15px 0'/>
        <Agree>
            <CheckBox/>
            <Overlay>Remember me ?</Overlay>
            <Span>Forgot password?</Span>
        </Agree>
        <Button width='100%' borderRadius='4px'>Sign in</Button>
        <Button width='100%' borderRadius='4px' background="none" color="#BFC8C5" margin="0 0 -40px 0">Registration</Button>
    </React.Fragment>
}
export default SignIn;