import styled from 'styled-components';
import React from 'react';
import Input from '../Input';
import Button from '../Button';

const Title = styled.h2`
    font:20px semibold;
    color:#15A748;
    text-transform:uppercase;
    margin-bottom:40px;
`;
const ForgetPassword = () =>{
    return <React.Fragment>
        <Title>FORGET PASSWORD ?</Title>
        <Input placeholder="E-mail Adress" margin='0 0 15px 0'/>
        <Button width='100%' borderRadius='4px'>SEND</Button>
        <Button width='100%' borderRadius='4px' background="none" color="#BFC8C5" margin="0 0 -40px 0">Back to sign in</Button>
    </React.Fragment>
}
export default ForgetPassword;