import styled from 'styled-components';

const BtnMainStyle = styled.button`
    font:16px semibold;
    color:#fff;
    background:#15A748;
    height:46px;
    padding:0 30px;
    cursor: pointer;
    outline:none;
    border:none;
    border-radius:6px;
`;
const Btn = styled(BtnMainStyle)(props => ({
    background: props?.background,
    color:props?.color,
    border:props?.border,
    margin:props?.margin,
    width:props?.width,
    borderRadius:props?.borderRadius,
}));

const Button = (props) => {
    const {color,background,margin,border,width,borderRadius,onClick} = props;
    return <Btn color={color}
            onClick={onClick}
            background={background} 
            margin={margin} 
            border={border}
            width={width}
            borderRadius={borderRadius}
            >
        {props.children}
    </Btn>
}

export default Button;